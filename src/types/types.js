

export const types =  {

    login:                  '[Auth] Login',
    logout:                 '[Auth] Logout',

    setPokemonList:         '[Pokemon] Set pokemon list',
    removePokemonList:      '[Pokemon] Remove pokemon list',
    setPokemon:             '[Pokemon] Set pokemon',
    removePokemon:          '[Pokemon] Remove pokemon',
    setPaginator:           '[Pokemon] Set Paginator list',
    removePaginator:        '[Pokemon] Remove Paginator list',

}
