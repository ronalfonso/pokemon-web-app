import {types} from "../types/types";


const initialState = {
    list: [],
    paginator: {
        next: '',
        previous: '',
        count: ''
    },
    details: {}
};

export const PokemonReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.setPokemonList:
            return {
                ...state,
                list: action.payload
            }

        case types.setPaginator:
            return {
                ...state,
                paginator: action.payload
            }

        case types.setPokemon:
            return {
                ...state,
                details: action.payload
            }

        default:
            return state
    }


}
