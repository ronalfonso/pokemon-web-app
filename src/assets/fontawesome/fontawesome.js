import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import {
    faChartPie, faChild, faCogs, faGlobe, faListUl,
    faPlusSquare,
    faTachometerAlt
} from "@fortawesome/free-solid-svg-icons";

library.add(
    fab,
    faPlusSquare,
    faTachometerAlt,
    faChartPie,
    faCogs,
    faChild,
    faGlobe,
    faListUl
)
