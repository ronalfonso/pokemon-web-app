import {PokemonServices} from "../api/pokemon.services";
import {types} from "../types/types";
import Swal from "sweetalert2";

export const startGetPokemonList = (url = '') => {
    Swal.fire({
        icon: 'info',
        text: 'Por favor espere...',
        allowOutsideClick: false,
    })
    Swal.showLoading();
    return (dispatch) => {
        let list = []
        PokemonServices.getListPokemon(url).then((resp) => {
            let {results, next, previous, count} = resp;
            next = next ? next.substr(33, 30) : null;
            previous = previous ? previous.substr(33, 30) : null;
            dispatch(setPaginator(next, previous, count))
            for (const pokemonItem of results) {
                let pokemon = {}
                for (let pokemonItemKey in pokemonItem) {
                    pokemon[pokemonItemKey] = pokemonItem[pokemonItemKey];
                    pokemon.id = pokemonItem.url.split('/')[6];
                    pokemon.picture = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${pokemon.id}.png`
                }
                PokemonServices.getPokemon(pokemon.id).then((result) => {
                    for (let resultKey in result) {
                        pokemon[resultKey] = result[resultKey];
                        if (list.indexOf(pokemon) < 0) {
                            list.push(pokemon)
                            dispatch(getPokemonList(list))
                        }
                    }
                })
            }
        })
    }
}

export const getPokemonList = (list) => {
    Swal.close();
    list.sort((a, b) => {
        if (a.name > b.name)  return 1;
        if (a.name < b.name)  return -1;
        return 0;
    })
    return {
        type: types.setPokemonList,
        payload: list
    }
}

export const removePokemonList = () => {
    return {
        type: types.removePokemonList
    }
}

export const setPaginator = (next, previous, count) => {
    return {
        type: types.setPaginator,
        payload: {
            next,
            previous,
            count
        }
    }
}

export const setPokemon = (pokemon) => {
    return {
        type: types.setPokemon,
        payload: pokemon
    }
}
