import {types} from "../types/types";
import Swal from "sweetalert2";
import {finishLoading, startLoading} from "./ui";
import {users} from "../shared/users";
import {Check} from "is-null-empty-or-undefined";

export const startLoginUserOrEmail = (usernameOrEmail, password) => {
    Swal.fire({
        icon: 'info',
        text: 'Por favor espere...',
        allowOutsideClick: false,
    })
    Swal.showLoading();
    return async (dispatch) => {
        dispatch(startLoading());
        const user = users.filter(ele => {
            return (ele.username === usernameOrEmail && ele.password == password) ||
                (ele.email === usernameOrEmail && ele.password == password)
        })[0]
        if (Check(user)) {
            Swal.fire({
                icon: 'error',
                title: 'Unauthorized',
                text: 'Dear user, your username and password do not match our records, please try again.!',
            }).then((result) => {
                if (result.isConfirmed) {
                    dispatch(finishLoading());
                }
            })
            return
        }
        for (let userKey in user) {
            localStorage.setItem(userKey, JSON.stringify(user[userKey]))
        }
        dispatch(login(user))

    }
}

export const login = (user) => {
    Swal.close();
    return {
        type: types.login,
        payload: user
    }
}

export const logout = () => {
    localStorage.clear();
    return {
        type: types.logout
    }
}
