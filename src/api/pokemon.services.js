import {BaseServices} from "./base.services";
import axios from "axios";


export class PokemonServices extends BaseServices {


    static getClassUrl() {
        return `${this.getUrl()}`;
    }

    static getListPokemon(url) {
        return axios.get(`${this.getClassUrl()}/pokemon/${url}`, null)
            .then(resp => {
                return resp.data;
            })
            .catch(err => {
                console.log(err);
                return [];
            })
    }

    static getPokemon(id) {
        return axios.get(`${this.getClassUrl()}/pokemon/${id}`, null)
            .then(resp => {
                return resp.data;
            })
            .catch(err => {
                console.log(err);
                return [];
            })
    }


}
