import {environment} from "../environments/environment.dev";

export class BaseServices {

    static API_V1 = environment.API_BASE_URL;

    static getBaseURl() {
        return BaseServices.API_V1;
    }

    static getClassUrl() {
        return ''
    }

    static getUrl() {
        return `${this.getBaseURl()}`
    }

}
