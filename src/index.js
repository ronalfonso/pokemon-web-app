import React from 'react';
import ReactDOM from 'react-dom';

import './styles.scss';
import PokemonApp from "./PokemonApp";

ReactDOM.render(
    <PokemonApp />,
  document.getElementById('root')
);

