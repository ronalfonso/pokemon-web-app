import React from 'react';
import {useHistory} from "react-router-dom";
import {Button} from "primereact/button";
import foto from '../../../assets/img/Ronald.jpg'
import {infoDeveloper, infoTecnology} from "../../../shared/data";

const DashboardScreen = () => {

    let history = useHistory();

    return (
        <div className="view-main">
            <div className="header-view">
                <div className="top-section__header-view">
                    <span className="title"><strong>Acerca de </strong></span>
                    <Button
                        className="btn-success__header-view"
                        label="Ver pokemon´s"
                        onClick={() => {
                            history.push("/pokemons")
                        }}
                    />
                </div>
                <div className="bottom-section__header-view">

                </div>
            </div>
            <div className="content-info">
                <div className="developer-info">
                    <div className="header">
                        <div className="left">
                            <h2>Ronald Alfonso</h2>
                            <h3>Desarrollador Fullstack</h3>
                        </div>
                        <div className="right">
                            <img src={foto} alt=""/>
                        </div>
                    </div>
                    <div className="content">
                        {
                            infoDeveloper.map(info => {
                                return (
                                    <div className="info">
                                        <div className="name">
                                            <span>{info.name}</span>
                                        </div>
                                        <div className="description">
                                            <span><strong>{info.Description}</strong></span>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
                <div className="project-info">
                    <div className="header">
                        <h2>Tecnologías y librerias</h2>
                        <h2>utilizadas</h2>
                    </div>
                    <div className="content">
                        {
                            infoTecnology.map(info => {
                                return (
                                    <div className="info">
                                        <div className="name">
                                            <span><strong>{info.name}</strong></span>
                                        </div>
                                        <div className="version">
                                            <span>{info.version}</span>
                                        </div>
                                        <div className="description">
                                            <span>{info.Description}</span>
                                        </div>
                                    </div>
                                )
                            })
                        }

                    </div>
                </div>
            </div>
        </div>
    );
};

export default DashboardScreen;
