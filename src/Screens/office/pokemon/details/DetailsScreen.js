import React from 'react';
import {useSelector} from "react-redux";
import {DataTable} from "primereact/datatable";
import {Column} from "primereact/column";
import {Button} from "primereact/button";
import {useHistory} from "react-router-dom";

const DetailsScreen = () => {

    const {details} = useSelector(state => state.pokemon);
    const history = useHistory();

    return (
        <>
            <div className="header-view" style={{marginBottom: '1.5rem'}}>
                <div className="top-section__header-view">
                    <span className="title"><strong>Detalles </strong></span>
                    <Button
                        className="btn-success__header-view"
                        label="Regresar"
                        onClick={() => {
                            history.push("/pokemons")
                        }}
                    />
                </div>
                <div className="bottom-section__header-view">

                </div>

            </div>
            <div className="details-pokemon">
                <div className="main-data">
                    <div className="left-data">
                        <div className="title">
                            <span>{details.name}</span>
                        </div>
                        <div className="content-data">
                            <div className="data">
                                <span className="subtitle">Especie</span>
                                <span><strong>{details.species.name}</strong></span>
                            </div>
                            <div className="data">
                                <span className="subtitle">Altura</span>
                                <span><strong>{details.height}</strong></span>
                            </div>
                            <div className="data">
                                <span className="subtitle">Peso</span>
                                <span><strong>{details.weight}</strong></span>
                            </div>
                            <div className="data">
                                <span className="subtitle">Experiencia base</span>
                                <span><strong>{details.base_experience}</strong></span>
                            </div>
                        </div>

                    </div>
                    <div className="right-data">
                        <div className="id-pokemon">
                            <span>#{details.id}</span>
                        </div>
                        <div className="pokebola-img">
                            <img src={details.picture} alt=""/>
                        </div>
                    </div>
                </div>
                <div className="supplementary-data">
                    <div className="ability">
                        <DataTable value={details.abilities} >
                            <Column field="ability.name" header="Habilidad" />
                            <Column field="slot" header="Slot" />
                        </DataTable>
                    </div>
                    <div className="forms">
                        <DataTable value={details.forms} >
                            <Column field="details.id" header="id" />
                            <Column field="name" header="Nombre" />
                        </DataTable>
                    </div>
                    <div className="types">
                        <DataTable value={details.types} >
                            <Column field="slot" header="Slot" />
                            <Column field="type.name" header="Tipo" />
                        </DataTable>
                    </div>
                    <div className="moves">
                        <DataTable value={details.stats} scrollable scrollHeight="220px">
                            <Column field="base_stat" header="Stat. Base" />
                            <Column field="effort" header="Esfuerzo" />
                        </DataTable>
                    </div>
                </div>
            </div>

        </>
    );
};

export default DetailsScreen;
