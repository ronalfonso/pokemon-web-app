import React from 'react';
import {
    Switch,
    Route,
    Redirect
} from "react-router-dom";
import PokemonListScreen from "./list/PokemonListScreen";
import DetailsScreen from "./details/DetailsScreen";

const PokemonRouter = () => {
    return (
        <div className="view-main">
            <Switch>

                <Route
                    exact
                    path="/pokemons"
                    component={PokemonListScreen}>
                </Route>

                <Route
                    exact
                    path="/pokemons/details"
                    component={DetailsScreen}>
                </Route>

                <Redirect to="/pokemon-list" />

            </Switch>
        </div>

    );
};

export default PokemonRouter;
