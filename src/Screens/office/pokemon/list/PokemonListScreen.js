import React, {useEffect, useState} from 'react';
import Tables from "../../../../components/tables/tables";
import {useDispatch, useSelector} from "react-redux";
import {startGetPokemonList} from "../../../../actions/pokemon";

const PokemonListScreen = () => {

    const dispatch = useDispatch();
    const [pokemonList, setPokemonList] = useState([]);
    const {list} = useSelector( state => state.pokemon);

    useEffect(() => {
        dispatch(startGetPokemonList());
    }, []);

    useEffect(() => {
        if (list.length > 0) {
            setPokemonList(list)
        }
    }, [list, setPokemonList]);

    const columns = [
        {field: 'name', header: 'Nombre', class: "color-image"},
        {field: 'species.name', header: 'Especie', class: "color-image"},
        {field: 'weight', header: 'Peso', class: "color-image"},
        {field: 'base_experience', header: 'Experiencia base', class: "color-image"}
    ];

    return (
        <>
            <div className="header-view">
                <div className="top-section__header-view">
                    <span className="title"><strong>Lista Pokemon</strong></span>
                </div>
                <div className="bottom-section__header-view">

                </div>

            </div>
            <div className="content-view">
                <Tables data={pokemonList} columns={columns} heightTable={'500px'}/>
            </div>
        </>
    );
};

export default PokemonListScreen;
