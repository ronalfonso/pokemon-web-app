export const infoTecnology = [
    {name: 'Reactjs', version: '17.0.1', Description: 'Framework javascript'},
    {name: 'Node', version: '14.15.4', Description: ''},
    {name: 'react-router-dom', version: '5.2.0', Description: 'Libreria de rutas para react'},
    {name: 'react-redux', version: '7.2.2', Description: 'Manejo de estado'},
    {name: 'react-thunk', version: '2.3.0', Description: 'Manejo del store'},
    {name: 'Axios', version: '0.21.1', Description: 'Peticiones http'},
    {name: 'PrimeReact', version: '6.2.1', Description: 'Libreria de estilos'},
    {name: 'Fontaewsome', version: '5.15.3', Description: 'Libreria de iconos'},
    {name: 'SweetAlert', version: '10.16.2', Description: 'Libreria para modales'},
    {name: 'is-null-empty-or-undefined', version: '1.0.1', Description: 'Comprobar contenido'},
]

export const infoDeveloper = [
    {name: 'Correo', Description: 'raalfonsoparra@gmail.com'},
    {name: 'Teléfono', Description: '+58 414-672-82.75'},
    {name: 'Años de experiencia', Description: '5 años'},
]
