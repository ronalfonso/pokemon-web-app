export const MenuSidebar = [
    {
        id: 1,
        name: 'Pokemon´s',
        permission: [],
        permissionBoolean: true,
        icon: 'globe',
        isSection: true,
        link: '',
        sections: [
            '/pokemons',
            '/pokemons/details'
        ],
        subsection: [
            {
                id: 1,
                name: 'Lista de pokemon',
                permission: [],
                icon: 'list-ul',
                isSection: false,
                link: '/pokemons',
                sections: [],
                subsection: [
                ]
            },
        ]
    },
    {
        id: 2,
        name: 'Desarrollo del sistema',
        permission: [],
        permissionBoolean: true,
        icon: 'cogs',
        isSection: true,
        link: '',
        sections: [
            '/dashboard'
        ],
        subsection: [
            {
                id: 1,
                name: 'Acerca de',
                permission: [],
                icon: 'child',
                isSection: false,
                link: '/dashboard',
                sections: [],
                subsection: [
                ]
            },

        ]
    },
];
