import React, {useState} from 'react';
import {DataTable} from 'primereact/datatable';
import {Column} from "primereact/column";
import {Ripple} from "primereact/ripple";
import {Dropdown} from "primereact/dropdown";
import {useDispatch, useSelector} from "react-redux";
import {setPokemon, startGetPokemonList} from "../../actions/pokemon";
import {Button} from "primereact/button";
import {useHistory} from "react-router-dom";

const Tables = ({data, columns, heightTable, optionButtons = null}) => {

    const dispatch = useDispatch();
    const history = useHistory();
    const {paginator: {count, next, previous}} = useSelector(state => state.pokemon);
    const [controlsPaginator, setControlsPaginator] = useState({
        first: 1,
        last: 20
    });
    const {first, last} = controlsPaginator;

    const imageBodyTemplate = (rowData) => {
        return <img className="imageTable" src={rowData.picture} alt="pokemon"/>
    }
    const dynamicColumns = columns.map((col, i) => {
        if (col.width) {
            return <Column className={`${col.class} td-capitalize`} key={col.field} field={col.field}
                           header={col.header} headerStyle={{width: col.width}}/>;
        } else {
            return <Column className={`${col.class} td-capitalize`} key={col.field} field={col.field}
                           header={col.header}/>;
        }
    });

    const handleDetails = (rowData) => {
        dispatch(setPokemon(rowData));
        history.push("/pokemons/details")
    }

    const actionBodyTemplate = (rowData) => {
        return (
            <React.Fragment>
                <Button icon="pi pi-eye" tooltip={'Details'} className="p-button-rounded p-button-info p-mr-2"
                        onClick={() => handleDetails(rowData)}/>
            </React.Fragment>
        )
    };

    const nextAction = () => {
        dispatch(startGetPokemonList(next))
        setControlsPaginator({
            first: first + 20,
            last: last + 20
        })
    }

    const previousAction = () => {
        dispatch(startGetPokemonList(previous))
        setControlsPaginator({
            first: first - 20,
            last: last - 20
        })
    }

    const template = {
        layout: 'RowsPerPageDropdown CurrentPageReport PrevPageLink NextPageLink',
        'PrevPageLink': (options) => {
            return (
                <button type="button" className={options.className} onClick={previousAction}
                        disabled={previous === null}>
                    <span className="p-p-3">Anterior</span>
                    <Ripple/>
                </button>
            )
        },
        'NextPageLink': (options) => {
            return (
                <button type="button" className={options.className} onClick={nextAction} disabled={next === null}>
                    <span className="p-p-3">Siguiente</span>
                    <Ripple/>
                </button>
            )
        },
        'RowsPerPageDropdown': (options) => {
            const dropdownOptions = [
                {label: 10, value: 10},
                {label: 20, value: 20},
                {label: 50, value: 50}
            ];

            return (
                <>
                    <span className="p-mx-1"
                          style={{color: 'var(--text-color)', userSelect: 'none'}}>Items por pagina: </span>
                    <Dropdown disabled={true} value={20} options={dropdownOptions} onChange={options.onChange}
                              appendTo={document.body}/>
                </>
            );
        },
        'CurrentPageReport': (options) => {
            return (
                <span style={{color: 'var(--text-color)', userSelect: 'none', width: '120px', textAlign: 'center'}}>
                    {first} - {last} of {count}
                </span>
            )
        }
    };

    return (
        <div className="table-content">
            <DataTable value={data} scrollable scrollHeight={heightTable}
                       paginator paginatorTemplate={template}
            >
                <Column className="color-image" header="Image" body={imageBodyTemplate}/>
                {dynamicColumns}
                <Column className="color-image" header="Opciones" headerStyle={{width: '160px'}}
                        body={actionBodyTemplate}/>
            </DataTable>
        </div>
    );
};

export default Tables;
